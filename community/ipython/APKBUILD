# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=ipython
pkgver=7.26.0
pkgrel=0
pkgdesc="A rich toolkit to help you make the most of using Python interactively"
options="!check" # Too many tests fail
url="https://ipython.org/"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-traitlets
	py3-pexpect
	py3-prompt_toolkit<3.1.0
	py3-pygments
	py3-pickleshare
	py3-decorator
	py3-backcall
	py3-simplegeneric
	py3-setuptools
	"
checkdepends="py3-pathlib2 py3-pytest py3-nose py3-matplotlib"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ipython/ipython/archive/$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	# Requires unpackaged 'testpath'
	rm -f IPython/core/tests/test_paths.py

	rm -f IPython/core/tests/test_completer.py

	# Requires unpackaged 'nbformat'
	rm -f IPython/core/tests/test_run.py

	py.test-3 \
		--deselect=IPython/terminal/tests/test_help.py::test_trust_help \
		--deselect=IPython/core/tests/test_display.py::test_set_matplotlib_formats_kwargs
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
ba76e11e9426694e6b542eaa495124a105b8697c547b5b926dd7cd7d4609e52f41d68509863286e508eb96ba6b5d7b1254448b941f207545e10241579d59ea61  ipython-7.26.0.tar.gz
"
